import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoDetallaComponent } from './destino-detalla.component';

describe('DestinoDetallaComponent', () => {
  let component: DestinoDetallaComponent;
  let fixture: ComponentFixture<DestinoDetallaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DestinoDetallaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoDetallaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
